from tkinter.messagebox import *
from tkinter import *
import string

root = Tk()
root.title("CHIFFRE DE CESAR")
root.minsize(400, 500)

decalage = 8

def changer_decalage():

    return label_decal.text(change_decalage.get())

def get_entree_content():

    return entree.get("0.0", END).strip()

def check_saisie():
        showwarning(title="ATTENTION",message="Ecrit un message!!")

def chiffrer():
    if(get_entree_content()==''):
        check_saisie()
    else:
        liste2 = []
        liste = get_entree_content()
        for i in get_entree_content():
            i = ord(i)+decalage
            liste2.append(chr(i))
            sortie.delete(0.0,END)
            sortie.insert(0.0,liste2)

def dechiffrer():
    if(get_entree_content()==''):
        check_saisie()
    else:
        liste2 = []
        liste = get_entree_content()
        for i in get_entree_content():
            i = ord(i)-decalage
            liste2.append(chr(i))
            sortie.delete(0.0,END)
            sortie.insert(0.0,liste2)

label_info = Label(text="Saisie ton texte a chiffrer/dechiffrer")
label_info.grid(row=1,column=1)

entree = Text(root,wrap='word', width=30,heigh=10)
entree.grid(row=2, column=1)

change_decalage = Entry(root,textvariable=decalage, width=30)
change_decalage.grid(row=3,column=2)

label_decal = Label(text=decalage)
label_decal.grid(row=2,column=2)

label_info = Label(text="Voici ton texte a chiffrer/dechiffrer")
label_info.grid(row=3,column=1)

sortie = Text(root,wrap='word', width=30,heigh=10)
sortie.grid(row=4, column=1)

btn_chiffrer = Button(root,text='Chiffrer',command=chiffrer)
btn_chiffrer.grid(row=4, column=0)

btn_dechiffrer = Button(root,text='Dechiffrer',command=dechiffrer)
btn_dechiffrer.grid(row=4, column=2)

root.rowconfigure(0, weight=1)
root.rowconfigure(1, weight=1)
root.rowconfigure(2, weight=1)
root.rowconfigure(3, weight=1)
root.rowconfigure(4, weight=1)
root.columnconfigure(0, weight=1)
root.columnconfigure(1, weight=1)
root.columnconfigure(2, weight=1)

root.mainloop()